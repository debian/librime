librime (1.3.1+dfsg1-1) unstable; urgency=medium

  * New upstream release.
  * Add myself into uploaders list.
  * Bump Standards-Version to 4.1.4 (no changes needed).
  * d/control: Migrate to Salsa platform for Vcs fields.
  * d/control: Restrict librime-data (brise) version to >= 0.38 to
    ensure users are using newer brise that is compatible with librime
    1.3+.
  * d/patches: Refresh patches and backport several upstream patches.
  * d/shlibs: Add a shlibs file to ensure link against latest librime
    library.

 -- Boyuan Yang <073plan@gmail.com>  Fri, 22 Jun 2018 20:42:21 +0800

librime (1.2.10+dfsg2-1) unstable; urgency=medium

  * Team upload.
  * New upstream release.
  * Remove more unused source code and regenerate +dfsg2 tarball.
  * d/patches: Backport some fixes, refresh patches.
  * d/copyright: Refresh copyright information.
  * d/control: Use debian-input-method maillist in Maintainer field.
  * d/control: Add libboost-locale-dev as build dependency.
  * d/rules: Install CHANGELOG.md as upstream changelog.
  * d/rules: Disable tests since GTest module is problematic.
  * d/control: Apply "wrap-and-sort -abst".
  * d/compat: Bump to debhelper compat v11.
  * d/watch: Use dversionmangle instead of uversionmangle.

 -- Boyuan Yang <073plan@gmail.com>  Sun, 25 Feb 2018 16:16:40 +0800

librime (1.2.9+dfsg2-1) unstable; urgency=medium

  * Team upload.
  * Rebuild the package and closes: #883373.
  * Remove zlib1.dll from source code and generate +dfsg2 tarball.
  * Bump Standards-Version to 4.1.3.
  * Use automatic debug packages, drop librime1-dbg.
  * Bump debhelper compat to v10.
  * d/rules: replace dh_install with the newer dh_missing.
  * d/rules: drop --parallel argument, no longer needed.
  * d/copyright: use secure url for copyright format, minor updates.
  * d/patches: add patch to turn on multiarch support for librime.
    Thus librime-dev is converted to arch:any.
  * d/control: mark librime1 as M-A: same.

 -- Boyuan Yang <073plan@gmail.com>  Thu, 28 Dec 2017 09:20:58 +0800

librime (1.2.9+dfsg1-1) unstable; urgency=medium

  * Imported Upstream version 1.2.9+dfsg1
    (Closes: #793255, #811637, #822122)
  * Bump opencc to >= 1.0.1
  * std-ver: 3.9.6 -> 3.9.8, no change
  * Update homepage to Github
  * Update VCS-Browser URL to HTTPS
  * Add -DBOOST_USE_CXX11=ON
  * Remove unneeded patch
  * Replace libkyotocabinet-dev with libleveldb-dev
  * Remove unneeded docs installation

 -- Aron Xu <aron@debian.org>  Mon, 08 Aug 2016 17:30:01 +0800

librime (1.2+dfsg-2) unstable; urgency=medium

  * Rebuild with libyaml-cpp0.5, 0.5.2. (Closes: #784199)
  * Updated copyright.
  * Bumped standard version to 3.9.6, no changes needed.

 -- Guo Yixuan (郭溢譞) <culu.gyx@gmail.com>  Fri, 08 May 2015 01:14:42 -0400

librime (1.2+dfsg-1) unstable; urgency=medium

  * New upstream release.
  * Build-dep on libmarisa-dev.
  * Added oldschool.diff to build without c++11. (Closes: #754199)
  * Updated copyright.

 -- Guo Yixuan (郭溢譞) <culu.gyx@gmail.com>  Sat, 19 Jul 2014 12:54:07 -0400

librime (1.1+dfsg-2) unstable; urgency=medium

  * debian/watch: mangle version for repacking.
  * debian/copyright: added Files-Excluded to remove binaries.
  * debian/control: added myself to uploaders.
  * Renamed librime1-dev back to librime-dev, as advised by the
    release team.

 -- Guo Yixuan (郭溢譞) <culu.gyx@gmail.com>  Wed, 02 Jul 2014 21:42:55 -0400

librime (1.1+dfsg-1) unstable; urgency=medium

  [ Guo Yixuan (郭溢譞) ]
  * New upstream release. (Closes: #746152)
  * librime1-dev: conflicts and replaces librime-dev, for
    renaming.
  * librime1-dev: dropped max version dependency on librime1.
    (Closes: #750623)
  * Bumped standard version to 3.9.5, no changes needed.

  [ Osamu Aoki (青木 修) ]
  * Removed binary blobs in thirdparty/{bin,src/opencc-windows}/ .
  * Updated copyright file.
  * Re-add max version dependency using Upstream-Version.

 -- Osamu Aoki <osamu@debian.org>  Sun, 29 Jun 2014 21:54:11 +0900

librime (1.0-1~exp1) experimental; urgency=low

  * New upstream release.
  * Bumped SOVERSION.

 -- Aron Xu <aron@debian.org>  Tue, 03 Dec 2013 02:58:47 +0800

librime (0.9.9-1) unstable; urgency=low

  * Imported Upstream version 0.9.9 (Closes: #724153)
  * Bump standard version to 3.9.4, no changes required.
  * Add a clean symbols file back:
     Only Rime* are symbols to export (Closes: #705902)
     Remove unused no-symbols-control-file
  * Update git url to canonical

 -- YunQiang Su <wzssyqa@gmail.com>  Fri, 11 Oct 2013 12:11:56 +0800

librime (0.9.8-2) unstable; urgency=low

  * Drop symbols file, as this package seems some chaos for symbols
    (Closes: #700988).
  * -DCMAKE_BUILD_TYPE=Release to disable lots logs (Closes: #702259).

 -- YunQiang Su <wzssyqa@gmail.com>  Wed, 20 Feb 2013 23:58:11 +0800

librime (0.9.8-1) unstable; urgency=low

  * New uptream release.
  * Add myself to Uploaders.

 -- YunQiang Su <wzssyqa@gmail.com>  Thu, 07 Feb 2013 16:25:20 +0800

librime (0.9.4-1) unstable; urgency=low

  * New upstream release.
  * Delete patches.
  * Add --fail-missing to rules.

 -- Qijiang Fan <fqj1994@gmail.com>  Wed, 26 Sep 2012 21:51:49 +0800

librime (0.9.2+git20120725-1) unstable; urgency=low

  * Initial release (Closes: #677341).

 -- Qijiang Fan <fqj1994@gmail.com>  Fri, 08 Jun 2012 18:04:26 +0800
